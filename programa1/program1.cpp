
#include <iostream>
#include <sys/wait.h>
#include <unistd.h>
#include <ctime>
#include "program1.h"

using namespace std;

//Constructor de la clase
program1::program1 (char *archive){
    this -> archive = archive;
    new_sp();
    count_howmany();
}

//Se crea el subproceso
void program1::new_sp(){
    pid = fork();
}

//Funcion que se encarga de contar la cantidad de lineas, caracteres y
//palabras de los archivos subidos por parte del usuario, usando el comando
//"wc", el cual vuelve las cantidades de "-l" lineas, "-m" caracteres, "-w" words
void program1::count_howmany(){
    if(pid < 0){
        cout << "Error al crear el subproceso." << endl;
    }
    else if(pid == 0){
        char* var[] = {"wc", this -> archive, "-l", "-m", "-w", NULL};
        execvp("wc", var);
        sleep(2);
    
    }else{
        wait(NULL);
    }
}


