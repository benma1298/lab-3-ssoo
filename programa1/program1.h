
#ifndef PROGRAM1_H
#define PROGRAM1_H

#include <iostream>
#include <unistd.h>
#include <ctime>

using namespace std;

//Se crea la clase con sus atributos
class program1{
    private:
    char *archive;
    pid_t pid;

    public:
    //Constructor de la clase
    program1(char *archive);
    float howmany_time(float time_i, float time_f);
    void new_sp();
    void count_howmany();
    
};

#endif