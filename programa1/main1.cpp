
#include <iostream>
#include <unistd.h>
#include <ctime>
#include <string.h>
#include "program1.h"

using namespace std;

// Funcion encargada de calcular e imprimir el tiempo de ejecucion retornando time_exe
float howmany_time(float time_i, float time_f){
    float time_exe = (time_f - time_i) / CLOCKS_PER_SEC;
    return time_exe;
}

//Funcion main del programa
int main(int argc, char **argv){
    if(argc < 2){
        cout << "Porfavor, ingrese 2 o mas atributos al programa." << endl;
        return -1;
    }
    //Toma el valor de time_i (tiempo inicial) del reloj
    clock_t time_i = clock();

    //Ciclo for por la cantidad de argumentos recibidos, llama a la funcion how_many
    cout << "A continuacion se mostrara el nombre junto a las cantidades de lineas, caracteres y palabras" << endl;
    cout << endl;
    for(int i = 1; i < argc; i++){
        cout << "/ Line / Char / Word / Name File" << endl;
        program1 how_many(argv[i]);
        cout << endl;
    }

    //Toma el valor de time_f (tiempo final) del reloj
    clock_t time_f = clock();
    
    //Imprimie el valor de time.exe (tiempo de ejecucion)
    cout << "El tiempo de ejecucion del programa fue de: " << howmany_time(time_i, time_f) << " segundos" << endl << endl;

    return 0;
}