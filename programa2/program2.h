
#ifndef PROGRAM2_H
#define PROGRAM2_H

#include <iostream>
#include <pthread.h>
#include <sys/wait.h>
#include <unistd.h>
#include <ctime>

using namespace std;

//Se crea la clase con sus atributos
class program2{
    private:
    char *archive;
    pid_t pid;

    public:
    //Constructor de la clase
    program2(char *archive);
    float howmany_time(float time_i, float time_f);
    void new_sp();
    void count_howmany();
    
};

#endif