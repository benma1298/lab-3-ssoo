
#include <iostream>
#include <pthread.h>
#include <ctime>
#include <unistd.h>
#include "program2.h"

using namespace std;

// Funcion encargada de calcular e imprimir el tiempo de ejecucion retornando time_exe
float howmany_time(float time_i, float time_f){
    float time_exe = (time_f - time_i) / CLOCKS_PER_SEC;
    return time_exe;
}

//Funcion main del programa
void *howmany(void *var){
    char *archive = (char *) var;
    program2 how_many(archive);
    sleep(2);
    pthread_exit(0);
}

int main(int argc, char **argv){
    
    if(argc < 2){
        cout << "Porfavor, ingrese 2 o mas atributos al programa." << endl;
        return -1;
    }
    //Toma el valor de time_i (tiempo inicial) del reloj
    clock_t time_i = clock();
    pthread_t threads[argc - 1];

    cout << "A continuacion se mostrara el nombre junto a las cantidades de lineas, caracteres y palabras" << endl;
    cout << endl;
    cout << "/ Lines / Characteres / Words / Name File" << endl;
    cout << endl;
    
    //For que crea los hilos del programa
    for(int i = 0; i < argc - 1; i++){

        pthread_create(&threads[i], NULL, howmany, argv[i+1]);
    }

    //For que espera la finalizacion de los hilos del programa
    for(int i = 0; i < argc - 1; i++){
        
        pthread_join(threads[i], NULL);
    }

    //Toma el valor de time_f (tiempo final) del reloj
    clock_t time_f = clock();
    cout << endl;
    //Imprimie el valor de time.exe (tiempo de ejecucion)
    cout << "El tiempo de ejecucion del programa fue de: " << howmany_time(time_i, time_f) << " segundos" << endl << endl;
    
    return 0;
}