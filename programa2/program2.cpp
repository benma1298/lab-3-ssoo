

#include <iostream>
#include <pthread.h>
#include <sys/wait.h>
#include <unistd.h>
#include <ctime>
#include "program2.h"

using namespace std;

//Constructor de la clase
program2::program2 (char *archive){
    this -> archive = archive;
    new_sp();
    count_howmany();
    pthread_exit(0);
}

//Se crea el subproceso
void program2::new_sp(){
    pid = fork();
}

//Funcion que se encarga de contar la cantidad de lineas, caracteres y
//palabras de los archivos subidos por parte del usuario, usando el comando
//"wc", el cual vuelve las cantidades de "-l" lineas, "-m" caracteres, "-w" words
void program2::count_howmany(){
    if(pid < 0){
        cout << "Error al crear el subproceso." << endl;
    }
    else if(pid == 0){
        char* var[] = {"wc", this -> archive, "-l", "-m", "-w", NULL};
        execvp("wc", var);
        sleep(1);
    
    }else{
        wait(NULL);
    }
}
