Sistemas operativos y Redes
Laboratorio 3
Hebras

Autor: Benjamin Martin Albornoz

Obejtivos:
    - Presentar el concepto de hebras, una unidad fundamental, utilizada por la CPU, conformando los
    fundamentos de los sistemas informaticos multihebras.
    - Explicar la API de la Biblioteca de hebras Pthreads.

Desarrollo:

El presente Laboratorio 3 consta de dos programas de codigo:

    El primer programa recibe a traves de los argumentos, una lista de archivos de texto, y cuenta la
cantidad de lineas, caracteres y palabras que existen en cada archivo subido por el usuario.

    El segundo programa se consigue modificando el primer programa para que posea un thread por cada
archivo de texto por analizar

En ambos programas se calcula, mediante las funciones correspondientes, el tiempo total de ejecucion
del programa

Compilacion:

    Para ejecutar el programa en cuestion, desde la terminal debemos abrir la carpeta de destino
en este casi: "cd lab3ssoo" seguido de abrir la carpeta correspondiente al programa1 o prorgama2
se la opcion que deciada el usuario, ejemeplo: "cd programa1" o "cd programa2"
    Cuando se encuentre en la carpeta deseada, se debe ejecutar el comando make, seguido del main del
programa a eleccion, junto con los archivos de texto que se desean analizar.
    
    Ejemplo programa1:  > make
                        > ./main1 prueba1.txt prueba2.txt

    Ejemplo programa2:  > make
                        > ./main2 prueba1.txt prueba2.txt

    El programa debe de recibir 2 archivos de entrada o mas.

